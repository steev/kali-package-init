Helper scripts to create new Debian packages
--------------------------------------------

debpin.py
----------

Fetches the source .tar.gz file, creates the packaging repository and populates the debian/ directory.

Autodetects the git forge being used (e.g. github, gitlab...) and creates a debian/watch file.

Requirements:
    apt-get install git debhelper

Usage:
    debpin.py <upstream repository URL>

Examples:
    debpin.py https://github.com/johnfactotum/foliate  # Create a Python package from a GitHub repository

See the help (-h) for details.

deb_create_watch.py
-------------------

Create a debian/watch file from a project URL using a set of popular git forge services.
